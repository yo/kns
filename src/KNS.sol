// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "./IKNS.sol";

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract KNS is IKNS, ERC1155 {
    error Unauthorized();
    error AlreadyRegistered();
    error WrongTLD();

    mapping(string => address) public lookup;
    mapping(address => string) public resolve;
    mapping(string => mapping(string => string)) texts;

    bytes32 klimaTLD =
        0xda0f65524ce07916ecad9cee57fb8b380bb2a6f8a63682e9ee3daecec3bdda15;

    constructor() ERC1155("") {}

    function getTokenId(string memory name) private pure returns (uint256) {
        return uint256(keccak256(bytes(name)));
    }

    function recordExists(string calldata name) external view returns (bool) {
        return lookup[name] != address(0);
    }

    function register(string calldata name, bytes32 tld) external payable {
        if (lookup[name] != address(0)) revert AlreadyRegistered();
        if (tld != klimaTLD) revert WrongTLD();

        string memory knsName = string(abi.encodePacked(name, ".klima"));

        lookup[knsName] = msg.sender;
        resolve[msg.sender] = knsName;

        // Mint NFT
        _mint(msg.sender, getTokenId(knsName), 1, "");

        emit NameRegistered(knsName, msg.sender);
    }

    function setName(string calldata name) external payable {
        if (msg.sender != lookup[name]) revert Unauthorized();

        resolve[msg.sender] = name;

        emit NameSet(name, msg.sender);
    }

    function setText(
        string calldata name,
        string calldata key,
        string calldata value
    ) external payable {
        if (msg.sender != lookup[name]) revert Unauthorized();

        texts[name][key] = value;

        emit TextChanged(name, key);
    }

    function text(string calldata name, string calldata key)
        external
        view
        returns (string memory)
    {
        return texts[name][key];
    }

    function burn(string calldata name) external payable {
        if (msg.sender != lookup[name]) revert Unauthorized();

        delete lookup[name];
        delete resolve[msg.sender];

        emit NameBurned(name, msg.sender);
    }
}
