// SPDX-License-Identifier: AGPL-3.0-only
pragma solidity ^0.8.11;

import "./Hevm.sol";
import "../KNS.sol";
import "ds-test/test.sol";

contract User {}

contract KNSTest is DSTest {
    User internal user;
    Hevm internal hevm;
    KNS internal kns;

    string name = "test";
    string knsName = "test.klima";
    bytes32 klimaTLD =
        0xda0f65524ce07916ecad9cee57fb8b380bb2a6f8a63682e9ee3daecec3bdda15;

    function setUp() public {
        user = new User();
        kns = new KNS();
        hevm = Hevm(HEVM_ADDRESS);
    }

    function testCanRegister() public {
        assertEq(kns.lookup(knsName), address(0));

        kns.register(name, klimaTLD);

        assertEq(kns.lookup(knsName), address(this));
        assertEq(kns.resolve(address(this)), knsName);
    }

    function testCannotRegisterExistingName() public {
        kns.register(name, klimaTLD);
        assertEq(kns.lookup(knsName), address(this));

        hevm.prank(address(user));
        hevm.expectRevert(abi.encodeWithSignature("AlreadyRegistered()"));
        kns.register(name, klimaTLD);

        assertEq(kns.lookup(knsName), address(this));
    }

    function testExists() public {
        kns.register(name, klimaTLD);

        assertTrue(kns.recordExists(name));
    }

    function testDoesnotExists() public {
        kns.register(name, klimaTLD);

        assertTrue(!kns.recordExists("yoginth"));
    }

    // Text Records
    function testCanSetText() public {
        kns.register(name, klimaTLD);
        assertEq(kns.lookup(knsName), address(this));

        string memory avatar = "https://yogi.codes/avatar.png";

        kns.setText(name, "avatar", avatar);

        assertEq(kns.text(name, "avatar"), avatar);
    }

    function testCannotSetText() public {
        kns.register(name, klimaTLD);
        assertEq(kns.lookup(knsName), address(this));

        string memory avatar = "https://yogi.codes/avatar.png";

        kns.setText(name, "avatar", avatar);

        hevm.prank(address(user));
        hevm.expectRevert(abi.encodeWithSignature("Unauthorized()"));
        kns.setText(name, "avatar", "https://sasi.codes/avatar.png");

        assertEq(kns.text(name, "avatar"), avatar);
    }
}
