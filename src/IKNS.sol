// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

interface IKNS {
    event NameRegistered(string indexed name, address indexed addr);
    event NameBurned(string indexed name, address indexed addr);
    event NameSet(string indexed name, address indexed addr);
    event TextChanged(string indexed name, string indexed key);

    function recordExists(string calldata name) external view returns (bool);

    function register(string calldata name, bytes32 tld) external payable;

    function setName(string calldata name) external payable;

    function setText(
        string calldata name,
        string calldata key,
        string calldata value
    ) external payable;

    function text(string calldata name, string calldata key)
        external
        view
        returns (string memory);

    function burn(string calldata name) external payable;
}
